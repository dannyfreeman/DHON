(import re)
(import pickle)
(import os)
(require [hy.contrib.loop [loop]])
(import [collections [Counter]])



;;;;;;;;;;;;;;
;;; Macros ;;;
;;;;;;;;;;;;;;

(defmacro map-l [f coll]
  "Wraps a call to map in a call to list"
  `(list (map ~f ~coll)))


(defmacro rest-l [coll]
  "Wraps a call to rest in a call to list"
  `(list (rest ~coll)))


(defmacro comment [&rest stuff]
  None)


(defmacro λ [args body]
  `(fn ~args ~body))


;;;;;;;;;;;;;;;;;
;;; Functions ;;;
;;;;;;;;;;;;;;;;;


(defn list-equals [x y]
  (= (Counter x) (Counter y)))


(defn None? [obj]
  (is obj None))

(defn one? [n]
  (= n 1))

(defn merge [&rest maps]
  (->> maps
       (reduce (fn [d1 d2] (merge-with (fn [x y] y) d1 d2)))))


(defn file-exists? [filename]
  (.isfile (. os path) filename))


(defn regex-match? [regex-string s]
  "(regex-match? regex string)"
  (-> (.compile re regex-string)
      (.match s)
      (None?)
      (not)))


(defn slurp [filename]
  "Reads in a pickled object"
  (.load pickle (open filename "rb")))


(defn spit [obj filename]
  "Writes an object to a pickle file"
  (.dump pickle obj (open filename "wb")))


(defn print-> [obj]
  "Prints an object and then returns it for use in threading macros."
  (print obj) obj)
