(require [extensions [*]])
(import [extensions [*]])

(comment "TEST"
         (import [math [log pow]])
         (->>
           (range -10 0)
           (map (fn [x] (pow 2 x)))
           (print->)
           (map log)
           (list)
           (+ [0])
           (print->)
           (map (fn [x] {:probability x}))
           (list)
           (adjust-result-probabilities True)))
(defn adjust-result-probabilities [normalize? test-results]
  (defn adjust []
    (defn get-largest []
      (->> test-results
           (map (fn [x] (:probability x)))
           (filter (fn [x] (!= x 0.0)))
           (list)
           ((fn [f] (if (empty? f)
                        0
                        (reduce max f))))))
    (setv largest (get-largest))
    (defn result-mapper [result]
      (setv p (:probability result))
      (if (zero? p)
          result
          (merge result {:probability (/ largest p)})))
    (if (zero? largest)
        test-results
        (->> test-results
             (map result-mapper)
             (list))))

  (if (not normalize?)
      test-results
      (adjust)))

(defn calc-correct-guess-percentage [experiment-results]
  (->> experiment-results
       (map (fn [r] (:correct? r)))
       (filter (fn [v] v))
       (list)
       (len)
       ((fn [l] (/ l (len experiment-results))))))
