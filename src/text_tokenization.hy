(require extensions)
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])
(import  re) ;; regular expressions
(import  [extensions [*]])

;;;;;;;;;;;;;;;;
;; Tokenizers ;;
;;;;;;;;;;;;;;;;

(defn tokenize-by-char [text &optional [transformer (fn [s] s)]]
  "Takes a full string of text and tokenizes it into a list of characters after applying the transformer to it."
  (->> text
       (transformer)
       (list)))


(defn tokenize-by-word [text &optional [transformer (fn [s] s)]]
  "Takes a full string of text and tokenizes it into a list of words after applying the transformer to it."
  (import  [nltk.tokenize [word-tokenize]])
  (->> text
       (transformer)
       (word-tokenize)))


(defn tokenize-tweet-by-word [text &optional [transformer (fn [s] s)]]
  (import [nltk.tokenize [TweetTokenizer]])
  (defn create-tokenizer []
    (TweetTokenizer :strip-handles False
                    :reduce-len True))
  (->> text
       (transformer)
       (.tokenize (create-tokenizer))))


(defn tokenize-tweet-by-char [text &optional [transformer (fn [s] s)]]
  (->> (tokenize-tweet-by-word text transformer)
       (.join " ")
       (list)))


;;;;;;;;;;;;;;;;;;;;;;;
;; Text transformers ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; Creates a transformer that removes punctuation, converts to lower case
;; (build-text-transfomer strip-punctuation-transfomer (fn [t] (.lower t)))

(defn compose-text-transformers [transform-functions]
  "Takes a list of functions, and returns a single function meant to be used in the tokenizing functions"
  (fn [text]
    (reduce (fn [transformed-text func] (func transformed-text)) transform-functions text)))


(defn strip-punctuation-transformer [text]
  "This should probably be used with the whitespace normalizer"
  (->> (.maketrans str "" "" "!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~«»¨©¦§")
       (.translate text)))


(defn strip-stopwords-and-whitespace-transformer [text]
  (import [nltk.corpus [stopwords]])
  (setv stops (.words stopwords "english"))
  (->> (.split text)
       (filter (fn [word] (not (in word stops))))
       (.join " ")))

(defn lower-case-transformer [text]
  (.lower text))


(defn normalize-url-transformer [text]
  (.sub re "https?:\/\/t.co\/[a-zA-Z0-9\-\.]{8}" "https://t.co/" text))


(defn normalize-handle-transformer [text]
  (.sub re "(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)" "@TwitterHandle" text))


(defn normalize-whitespace-transformer [text]
  (->> text
       (.split)
       (.join " ")))


;;;;;;;;;
;; OLD ;;
;;;;;;;;;

(defn -replace-in-tweet [tweet-word-list substitutions]
  "This must replace tweets based on what is in the substitutions"
  (loop [[subs substitutions] [tweet tweet-word-list]]
        (defn replacement-mapper [word]
          (if (regex-match? (first ((first subs))) word)
            (second (first subs))
            word))
        (if (empty? subs)
          (list tweet)
          (recur (rest-l subs) (map replacement-mapper tweet)))))


(defn tokenize-tweet [tokenizer-fn tweet &optional [substitutions []]]
  (-> (tokenizer-fn tweet)
      (-replace-in-tweet substitutions)))


(defn tokenize-tweet-coll [tokenizer-fn tweet-coll &optional [substitutions []]]
  (defn mapper [t]
    (tokenize-tweet tokenizer-fn t substitutions))
  (list (map mapper tweet-coll)))


(defn create-NLTK-tweet-tokenizer [tweet]
  (import [nltk.tokenize [TweetTokenizer]])
  (defn create-tokenizer []
    (TweetTokenizer :preserve-case True
                    :strip-handles False
                    :reduce-len True))
  (.tokenize (create-tokenizer) tweet))


(defn create-NLTK-char-tokenizer [tweet]
  (.tokenize (CharTokenizer) (.lower tweet)))


(defn tokenize-coll-as-NLTK-tweet [tweet-coll &optional [substitutions []]]
  (tokenize-tweet-coll create-NLTK-tweet-tokenizer tweet-coll substitutions))



(defn get-url-substitution []
  ["https?:\/\/t.co\/[a-zA-Z0-9\-\.]{8}" "https://t.co/"])


(defn get-handle-substitution []
  ["(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)" "@TwitterHandle"])


(defn get-all-substitutions []
  [(get-url-substitution) (get-handle-substitution)])


;; (defn tokenize-file [filename &optional [normalize-urls True] [normalize-handles True]]
;;   "By default, will normalize URL's and Usernames in a tweet."
;;   (setv replacements [(if normalize-urls (get-url-substitution))
;;                       (if normalize-handles (get-handle-substitution))])
;;   (tokenize-coll-as-NLTK-tweet (-read-file filename) replacements))


;; (defn tokenize-tweet-default [tweet]
;;   (tokenize-tweet create-NLTK-tweet-tokenizer tweet (get-all-substitutions)))


;; (defn tokenize-tweet-char [tweet]
;; (tokenize-tweet create-NLTK-char-tokenizer tweet (get-all-substitutions)))
