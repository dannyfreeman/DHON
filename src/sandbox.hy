(import [math [*]])
(import [sys [*]])

(defn test []
  (setv small-vals (->> (range -50 -30)
                        (map (fn [i] (pow 10.456 i)))
                        (list)
                        (sorted)))

  (print "These are the really small values")
  (print small-vals)
  (print "they are ordered: " (= (sorted (.copy small-vals)) small-vals))

  (setv largest (->> small-vals
                     (map (fn [i] (log i)))
                     (last)))

  (setv normalized-vals (->> small-vals
                             (map (fn [i] (/ largest (log i))))
                             (list)))

  (print "These are the values after their magnitude is accounted for")
  (print normalized-vals)
  (print "They are still in the correct order: "(= (sorted (.copy normalized-vals)) normalized-vals)))

(test)
