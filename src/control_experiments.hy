(require [extensions [*]])
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])
(import [markov-chains [*]])
(import [text-tokenization [*]])
(import [experiment-helpers [*]])

;; https://pypi.python.org/pypi/Gutenberg
(import [gutenberg.acquire [load-etext get-metadata-cache]])
(import [gutenberg.query [get-metadata list-supported-metadatas get-etexts]])
(import [gutenberg.cleanup [strip-headers]])
(import [random [randint]])

;;;;;;;;;;;;;;;;;;;;;;
;; Global Variables ;;
;;;;;;;;;;;;;;;;;;;;;;

;; Might be short, not actual copies
;; A Connecticut Yankee in King Arthur's Court
;; The Soul of Man under Socialism
;; Charmides, and Other Poems

(def authors {"Melville, Herman" ["Typee: A Romance of the South Seas" "Battle-Pieces and Aspects of the War" "The Piazza Tales"]
              "Austen, Jane" ["Sense and Sensibility" "Emma" "Pride and Prejudice" "Persuasion"]
              "Shakespeare, William" ["Hamlet, Prince of Denmark" "A Midsummer Night's Dream" "The Taming of the Shrew"]
              "Carroll, Lewis" ["Alice's Adventures in Wonderland" "Through the Looking-Glass" "The Hunting of the Snark: An Agony in Eight Fits"]
              "Doyle, Arthur Conan" ["The Adventures of Sherlock Holmes" "The Tragedy of the Korosko" "The Firm of Girdlestone" "The New Revelation"]
              "Dickens, Charles" ["Oliver Twist" "A Christmas Carol" "The Seven Poor Travellers"]
              "Twain, Mark" ["The Prince and the Pauper" "A Tramp Abroad" "A Horse's Tale"]
              "Wilde, Oscar" ["The Soul of Man under Socialism" "The Picture of Dorian Gray" "Charmides, and Other Poems"]
              "Irving, Washington" ["The Sketch-Book of Geoffrey Crayon" "Tales of a Traveller" "Bracebridge Hall, or The Humorists"]})


;;;;;;;;;;;;;;;
;; Functions ;;
;;;;;;;;;;;;;;;

(defn -get-transformer [experiment-num]
  "Valid values are 1 through 7"
  (setv control-transformers
        [normalize-whitespace-transformer
         (compose-text-transformers [lower-case-transformer normalize-whitespace-transformer])
         (compose-text-transformers [lower-case-transformer strip-punctuation-transformer normalize-whitespace-transformer])
         (compose-text-transformers [strip-punctuation-transformer normalize-whitespace-transformer])
         (compose-text-transformers [strip-stopwords-and-whitespace-transformer strip-punctuation-transformer])
         (compose-text-transformers [strip-stopwords-and-whitespace-transformer lower-case-transformer])
         (compose-text-transformers [strip-stopwords-and-whitespace-transformer lower-case-transformer strip-punctuation-transformer])])
  (get control-transformers (dec experiment-num)))


(defn -get-book-title-list [author]
  ;; Gets a list of all the books titles for an author
  (->> (get-etexts "author" author)
       (map (fn [num] (first (get-metadata "title" num))))
       (list)))


(defn -get-book-number-dict [book-title-coll]
  "Take a list of title, and uses them as keys mapping to their gutenberg ID"
  (dict-comp t (first (get-etexts "title" t)) (t book-title-coll)))


(defn -get-book-text [gutenberg-id]
  "Gets the text of a book by it's gutenberg ID"
  (->> (load-etext gutenberg-id)
       (strip-headers)
       (.strip)))


(defn -get-book-text-by-title [title]
  "Gets the text of a book by its title"
  (loop [[ids (-> (get-etexts "title" title) (list))]
         [text None]]
        (if (not (None? text))
            text
            (if (empty? ids) ;; if all downloads failed, throw an exception
                (raise (Exception (+ "Could not download" title)))
                (do ;; if there are still id's left, download them
                  (try
                    (recur (list (rest ids)) (-get-book-text (first ids)))
                    (except [Exception]
                      (recur (list (rest ids)) None))))))))


(defn -title-to-markov-chain [title transformer tokenizer]
  "Takes a title of a book and a transformer and returns a Markov Chain."
  (-> (-get-book-text-by-title title)
      (transformer)
      (tokenizer)
      (create-markov-chain-single-text {} True)))


(defn -create-control-markov-chains [author-title-map transformer tokenizer]
  ;; Takes in a data structure that looks like
  ;; {"author"  ["book1" "book2" "book3"]
  ;;  "author2" ["book1"]
  ;;  ....}
  ;; And returns a list that looks like this
  ;; [{:author "Author" :title "Book Title" :text "Book Text" :markov-chain Markov-Chain}
  ;;  {:author "Author" :title "Book Title" :text "Book Text" :markov-chain Markov-Chain}
  ;;  ...]
  (loop [[keys (-> (.keys author-title-map) (list))]
         [book-list []]]
        (defn mapper [book]
          {:author (first keys)
           :title book
           :text (->> (-get-book-text-by-title book)
                      (drop 500)
                      (take (* 140 3000))
                      (list)
                      (.join ""))

           :markov-chain (-title-to-markov-chain book transformer tokenizer)})
        (if (empty? keys)
            book-list
            (recur (list (rest keys))
                   (+ book-list (->> (first keys) (get author-title-map) (map mapper) (list)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Experiment Functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment
  (->> (run-experiment 1 100 100 get-probability tokenize-by-char)
       (first)
       (:best-guess)))

(defn run-experiment [experiment-num excerpt-start excerpt-length probability-fn tokenizer]
  ;; probability-fn takes in a Markov Chain and
  ;; a list of Tokenized text, and returns a probability
  (defn take-random-excerpt [book-text]
    (if (None? excerpt-length)
        book-text
        (->> (-> (randint excerpt-start (- (len book-text) excerpt-length))
                 (drop book-text))
             (take excerpt-length)
             (list))))

  (defn create-probability-mapper [excerpt]
    (fn [current-book]
      {:author (get current-book :author)
       :title (get current-book :title)
       :probability (probability-fn (:markov-chain current-book) excerpt)}))

  (defn get-best-guess [probability-results]
    (setv best (->> probability-results
                    (map (fn [res] (get res :probability)))
                    (list)
                    (max)))
        (->> (filter (fn [x] (= (get x :probability) best)) probability-results)
             (list)
             (first)))

  ;; Get the transformer to use
  (setv transformer (-get-transformer experiment-num))

  ;; Create the markov chains for each book
  (setv markov-chains (-create-control-markov-chains authors transformer tokenizer))

  (defn experiment-mapper [book]
    ;; Grabs a random excerpt from book,
    ;; then maps the excerpt to the probability analysis results
    (setv book-text (->> (:text book)
                         (transformer)
                         (tokenizer)))
    (setv random-excerpt (take-random-excerpt book-text))

    ;; The book the current excerpt is taken from is removed
    ;; before the probabilities are calculated.
    (setv probabilities (->> markov-chains
                             (filter (fn [a] (!= (:title book) (:title a))))
                             (map (create-probability-mapper random-excerpt))
                             (list)
                             (adjust-result-probabilities (= probability-fn get-probability))))
    (setv best-guess (get-best-guess probabilities))
    ;; False if wrong author, guess is 0 probability, or all probabilities are 1
    (setv correct? (and (= (:author book) (:author best-guess))
                        (not (zero? (:probability best-guess)))
                        (not (->> (map (fn [r] (:probability r)) probabilities)
                                  (map one?)
                                  (reduce and)))))
    {:author (:author book)
     :title (:title book)
     :excerpt random-excerpt
     :probabilities probabilities
     :best-guess best-guess
     :correct? correct?})

  (->> (map experiment-mapper markov-chains)
       (list)))


(defn run-full-experiment [start-offset excerpt-lengths &optional [probability-fn get-probability] [tokenizer tokenize-by-char]]
  "Takes in a list of excerpt-lengths and runs a full suite of experiments,
   printing a table with the average result of each experiment for each control group"

  (defn get-avg-result [experiment-num length]
    "Runs an experiment function with an excerpt length 5 times, then averages the results."
    (->> (repeatedly (fn [] (run-experiment experiment-num start-offset length probability-fn tokenizer)))
         (take 5)
         (map calc-correct-guess-percentage)
         (reduce +)
         ((fn [a] (/ a 5.0)))))

  (defn length-mapper [excerpt-len]
    "Maps a list of exert lengths into the average of experiment results."
    (->> (range 7)
         (map inc)
         (map (fn [func] (.format str "{0:.6f} | " (get-avg-result func excerpt-len))))
         (list)
         (reduce +)
         (print "|  " excerpt-len " | ")))

  (print "#+ATTR_LaTeX: :center nil")
  (print "#+PLOT: title:\"INSERT TITLE HERE\" ind:1 with:lines set:\"yrange [0:1]\" set:\"xlabel 'Excerpt Length\" set:\"ylabel 'Accuracy'\" set:\"output 'INSERT NAME HERE.png'\"")
  (print "| length | control1 | control2 | control3 | control4 | control5 | control6 | control7 |")
  (print "|--------+----------+----------+----------+----------+----------+----------+----------|")
  (->> excerpt-lengths
       (map length-mapper)
       (list)))


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Module Main Function ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmain [&rest args]
  (setv num (int (second args)))
  (cond [(= num 1) (run-full-experiment 100 [100 140 200 250 300 350 400] get-probability)]
        [(= num 2) (run-full-experiment 100 [100 250 500] get-probability)]
        [(= num 3) (run-full-experiment 100 [100 140 250 500 750 1000 2000] get-percent-text-is-represented tokenize-by-char)]
        [(= num 4) (run-full-experiment 100 [100 250 500 750 1000 1250 1500 1750 2000] get-percent-text-is-represented tokenize-by-word)]
        [(= num 5) (run-full-experiment 100 [None] get-percent-text-is-represented tokenize-by-word)]
        [True (print "Enter number between 1 and 4")]))
