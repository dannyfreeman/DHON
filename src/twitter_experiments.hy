(import [extensions [*]])
(import [text-tokenization [*]])
(import [markov-chains [*]])
(import [experiment-helpers [*]])
(import [tweet-getter [tweet-dir handles]])
(import random)

(require [extensions [comment]])
(require [hy.extra.anaphoric [*]])


;;;;;;;;;;;;;;;;;;;;;;
;; Global Variables ;;
;;;;;;;;;;;;;;;;;;;;;;


(def -twitter-text-transformers
  [normalize-whitespace-transformer
   (compose-text-transformers [lower-case-transformer normalize-whitespace-transformer])
   (compose-text-transformers [strip-stopwords-and-whitespace-transformer])
   (compose-text-transformers [strip-stopwords-and-whitespace-transformer lower-case-transformer])
   (compose-text-transformers [normalize-whitespace-transformer normalize-handle-transformer normalize-url-transformer])
   (compose-text-transformers [normalize-whitespace-transformer normalize-handle-transformer normalize-url-transformer lower-case-transformer])
   (compose-text-transformers [normalize-whitespace-transformer normalize-handle-transformer normalize-url-transformer strip-stopwords-and-whitespace-transformer])
   (compose-text-transformers [normalize-whitespace-transformer normalize-handle-transformer normalize-url-transformer strip-stopwords-and-whitespace-transformer lower-case-transformer])])


;;;;;;;;;;;;;;;
;; Functions ;;
;;;;;;;;;;;;;;;


(defn -get-transformer [group-number]
  ;; Get the text-transformer for group-number
  (get -twitter-text-transformers (dec group-number)))


(defn -read-all-pickled-tweets []
  "Returns a map of handles and tweet collection"
  (comment {"handle1" ["t1" "t2" "..."]
            "handle2" ["t1" "t2" "..."]
            "..." ["..."]})
  (->> ;; real-twitter-accounts
       ;; tweet-dir from tweet-getter module
       handles
       (map (fn [handle] [handle (+ tweet-dir handle ".pickle")]))
       (map (fn [pair] {(first pair) (slurp (second pair))}))
       (apply merge)))


;; This seems to be working, here's a test
(comment
  (-tweets-to-markov-chain (get (-read-all-pickled-tweets) "dhh") (fn [s] s) tokenize-by-word))
(defn -tweets-to-markov-chain [tweets transformer tokenizer]
  (->> (map transformer tweets)
       (map tokenizer)
       (list)
       (create-markov-chain)))

(defn -take-random-tweet [tweets]
  (setv tweet (.choice random tweets))
  (if (empty? tweet)
      (-take-random-tweet tweets)
      tweet))


(comment
  (setv r1 (-run-experiment (-read-all-pickled-tweets) 1 tokenize-by-char get-probability))
  (setv handle (->> r1
                    (filter (fn [r] (= (:handle r) "realDonaldTrump")))
                    (first)))
  (->> handle (:tweets) (len))
  (->> handle (:probabilities))
  (->> handle (:best-guess))
  (->> handle (:correct?))
  (->> handle (:handle))
  (->> handle (:random-tweet) (.join "")))

(defn -run-experiment [tweet-corpus group-num tokenizer probability-fn]
  (setv transformer (-get-transformer group-num))

  (defn handle-mapper [handle]
    "Takes a twitter handle and creates a data structure containing the handle,
     a random tweet, a collection of all the handle's tweets, and a Markov Chain
     representing all the tweets."
    (setv tweets (get tweet-corpus handle))
    (setv random-tweet (->> (-take-random-tweet tweets)
                            (transformer)
                            (tokenizer)))
    (setv tweets (->> tweets
                      (remove (fn [t] (= t random-tweet)))
                      (list)))

    ;; The returned data structure
    {:handle handle
     :random-tweet random-tweet
     :tweets tweets
     :markov-chain (-tweets-to-markov-chain tweets transformer tokenizer)})

  ;; Creates a complete list of the data structures created by handle-mapper
  (setv twitter-markov-chain-map (->> tweet-corpus
                                      (map handle-mapper)
                                      (list)))

  (defn experiment-mapper [handle-map]
    "Maps elements in twitter-markov-chain-map to a data structure similar to the one
     created by handle mapper, but with the experiment results merged in. They are
     :probabilities [{:handle handle1 :probability p1}
                     {:handle handle2 :probability p2}]
     :best-guess {:handle n}
     :correct? True/False"

    (defn probability-mapper [e]
      "Takes an entry from the twitter-markov-chain-map, and maps the probability
       that each chain could generate the random tweet from handle-map"
      {:handle (:handle e)
       :probability (probability-fn (:markov-chain e) (:random-tweet handle-map))})

    ;; Calculate all the probabilities that a tweet could be produced from each Markov Chain
    ;; in the corpus
    (setv probabilities (->> (map probability-mapper twitter-markov-chain-map)
                             (list)
                             (adjust-result-probabilities (= probability-fn get-probability))))

    (defn best-guesser [p1 p2]
      (if (>= (:probability p1) (:probability p2))
          p1
          p2))

    ;; Find the handle with the greatest probability of generating the random tweet.
    (setv best-guess (->> probabilities
                          (reduce best-guesser)))

    ;; Merge the results of the experiments with the handle map
    (merge handle-map {:probabilities probabilities
                       :best-guess best-guess
                       :correct? (and (= (:handle best-guess) (:handle handle-map))
                                      (not (zero? (:probability best-guess))))}))

  ;; Kick off the experiment
  (->> twitter-markov-chain-map
       (map experiment-mapper)
       (list)))


(comment (print-results tokenize-by-char get-probability))
(defn print-results [tokenizer probability-fn]
  (setv tweet-corpus (-read-all-pickled-tweets))
  (setv number-of-tests 10)
  (setv n [1])

  (defn runner [i]
    (->> (repeatedly (fn [] (-run-experiment tweet-corpus i tokenizer probability-fn)))
         (take number-of-tests)
         (map calc-correct-guess-percentage)
         (reduce +)
         ((fn [a] (/ a number-of-tests)))))

  (defn printer [res]
    (print (.format str "|        {0} |     {1:.3f} |" (first n) res))
    (assoc n 0 (inc (first n))))

  (print "#+PLOT: title:\"TITLE HERE\" with:boxes ind:1 set:\"yrange [0:1]\" set:\"xlabel 'Group Number\" set:\"ylabel 'Average Accuracy'\" set:\"output 'twitterN.png'\" set:\"boxwidth 0.5\" set:\"style fill solid\"")
  (print "| *Group#* | *Average* | ")
  (print "|----------+-----------| ")
  (->> (range 1 (inc (len -twitter-text-transformers)))
       (map runner)
       (map printer)
       (list)))


(defn print-all []
  (print-results tokenize-by-char get-probability)
  (print)
  (print-results tokenize-by-word get-probability)
  (print)
  (print-results tokenize-by-char get-percent-text-is-represented)
  (print)
  (print-results tokenize-by-word get-percent-text-is-represented))


(defmain [&rest args]
  (setv num (int (second args)))
  (cond [(= num 1) (print-results tokenize-by-char get-probability)]
        [(= num 2) (print-results tokenize-by-word get-probability)]
        [(= num 3) (print-results tokenize-by-char get-percent-text-is-represented)]
        [(= num 4) (print-results tokenize-by-word get-percent-text-is-represented)]
        [(= num 0) (print-all)]
        [True (print "Enter number between 0 and 4")]))
