(import [api_key [*]])
(import tweepy)
(import [extensions [*]])
(import [user-lists [*]])
(import pickle)
(import os.path)

(require [extensions [*]])
(require [hy.contrib.loop [*]])

;;;;;;;;;;;;;;;;;;;;;;
;; Global Variables ;;
;;;;;;;;;;;;;;;;;;;;;;


(def tweet-dir "tweets/journalists/")

(def handles journalist-twitter-accounts)

;;;;;;;;;;;;;;;
;; Functions ;;
;;;;;;;;;;;;;;;


(defn -create-pickled-tweets-name [handle]
  (defn -make-path [directory filename extension]
    (->> (+ directory filename extension)
         (.abspath (. os path))))
  (-make-path tweet-dir handle ".pickle"))


(defn -get-api []
  "Gets an api object with this application's twitter keys"
  (setv authenticator (.OAuthHandler tweepy (:ConsumerKey secret-keys)
                                            (:ConsumerSecret secret-keys)))
  (setv authenticator.secure True)
  (.set_access_token authenticator (:AccessToken secret-keys)
                                   (:AccessTokenSecret secret-keys))
  (.API tweepy
        :auth_handler authenticator
        :wait_on_rate_limit True
        :wait_on_rate_limit_notify True))


(defn -extract-tweets-from-status [tweets]
  "Given a list of tweepy.status objects, extracts tweet texts"
  (->> tweets
       (map (fn [t] (.encode t.text "utf-8")))
       (list)))


(defn -get-cursor [handle]
  "Gets a cursor that iterates of all the tweets posted by screen-name"
  (->> (.Cursor tweepy (. (-get-api) user_timeline)
                :screen_name handle
                :include_rts False)
        (.items)))


(defn -get-tweets [handle]
  "Uses a cursor with a limit handler to scrape tweets"
  (loop [[tweets []] [cursor (-get-cursor handle)]]
        (try (recur (cons (next cursor) tweets) cursor)
             (except [e StopIteration]
               (-extract-tweets-from-status tweets)))))


(defn -dump-tweets-to-file [tweets filename]
  "Serializes a tweet collection in a file."
  (-> (->> tweets
           (map (fn [t] (.decode t "utf-8")))
           (list))
      (spit filename)))


(defn handle-exists? [handle]
  (try
    (do (.get-user (-get-api) handle)
        True)
    (except (tweepy.TweepError)
      False)))


(defn scrape-and-pickle [handle &optional [redownload? False]]
  "Scrapes and pickles data for a twitter handle."
  (setv filename (-create-pickled-tweets-name handle))
  (if (and (handle-exists? handle)
           (or redownload?
               (not (file-exists? filename))))
      (do
        (print (+ "Downloading tweets for " handle))
        (setv tweets (-get-tweets handle))
        (-dump-tweets-to-file tweets filename))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mass Tweet Downloading Functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn scrape-and-pickle-handles [handle-list &optional [redownload? False]]
  "Scrapes and pickles data for a list of twitter handles"
  (->> (map (fn [h] (scrape-and-pickle h redownload?)) handle-list)
       (list)))


(defmain [&rest args]
  (print "Downloading a lot of data, please wait . . .")
  (scrape-and-pickle-handles handles (-> (second args) (str) (.lower) (= "true"))))
