#+TITLE: DHON Project
#+AUTHOR: Danny Freeman

This is my departmental honors thesis that I completed to eacn my Bachelor's degree in computer science.

See [[https://gitlab.com/dannyfreeman/DHON/blob/master/thesis/thesis.org][thesis.org]] to learn about the project.
